#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <libgen.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

// C Custom Header File
#include "list.h"
#include "log.h"
#include "lsusb-telit.h"

#define MY_SYSFS_FILENAME_LEN 255
#define MY_PATH_MAX 4096
#define MY_PARAM_MAX 64

static const struct GEnumValueShort MMBroadbandModemTelit[] = {
    {unknown, "unknown"},
    {TELIT_ME910C1_FAMILY, "ME910C1"},
    {TELIT_MEx10G1_FAMILY, "MEx10G1"},
    {TELIT_LE910Bx_FMAILY, "LE910Bx"},
    {TELIT_LE910Cx_FAMILY, "LE910Cx"},
    {TELIT_LM940A11_FAMILY, "LM940A11"},
    {TELIT_LM960A18_FMAILY, "LM960A18"},
    {TELIT_LN940A9_FAMILY, "LN940A9"},
    {TELIT_LN960A16_FAMILY, "LN960A16"},
    {TELIT_FN980_FAMILY, "FN980m"}};

typedef enum {
  MM_MODEM_PORT_TYPE_UNKNOWN = 0,
  MM_MODEM_PORT_TYPE_NET,
  MM_MODEM_PORT_TYPE_AT,
  MM_MODEM_PORT_TYPE_ATPPP,
  MM_MODEM_PORT_TYPE_DIAG,
  MM_MODEM_PORT_TYPE_TRACE,
  MM_MODEM_PORT_TYPE_GPS,
  MM_MODEM_PORT_TYPE_QMI,
  MM_MODEM_PORT_TYPE_MBIM,
  MM_MODEM_PORT_TYPE_AUDIO,
  MM_MODEM_PORT_TYPE_ECM,
  MM_MODEM_PORT_TYPE_NCM,

  MM_MODEM_PORT_TYPE_CDCWDM,
} MMModemPortType;

static const char *MMModemPortType_String[] = {
    "unknown",
    "net",
    "at",
    "at",
    "diag",
    "trace",
    "gps",
    "qmi",
    "mbim",
    "audio",
    "net",
    "net"};

static unsigned int usbdevice_count = 0;
static unsigned int passing_fastenum = 0;
static unsigned int is_first_bootup = 0;

struct usbsysfs {
  struct list_head list;
  struct usbdevice *next;

  char syspath[MY_SYSFS_FILENAME_LEN];
  char d_name[MY_SYSFS_FILENAME_LEN];
};

struct interface {
  unsigned int index;
  unsigned int type;
  char driver[MY_PARAM_MAX];
  char name[MY_PARAM_MAX];
  char network[MY_PARAM_MAX];
};

struct usbdevice {
  struct list_head list;  /* connect devices independent of the bus */
  struct usbdevice *next; /* next port on this hub */

  unsigned int index;
  unsigned int antennas;
  MMBoardbandModem_Telit type;
  unsigned int bConfigurationValue;
  unsigned int bNumConfigurations;
  unsigned int bNumInterfaces;
  unsigned int idProduct;
  unsigned int idVendor;
  char manufacturer[MY_PARAM_MAX];
  char product[MY_PARAM_MAX];
  char serial[MY_PARAM_MAX];
  char version[MY_PARAM_MAX];
  char speed[MY_PARAM_MAX]; /* '1.5','12','480','5000' + '\n' */

  struct interface *intf;
  char name[MY_SYSFS_FILENAME_LEN];
};

struct usbdevice *s_active_module = {0};

#define SYSFS_INTu(de, tgt, name)                   \
  do {                                              \
    tgt->name = read_sysfs_file_int(de, #name, 10); \
  } while (0)
#define SYSFS_INTx(de, tgt, name)                   \
  do {                                              \
    tgt->name = read_sysfs_file_int(de, #name, 16); \
  } while (0)
#define SYSFS_STR(de, tgt, name)                                \
  do {                                                          \
    read_sysfs_file_string(de, #name, tgt->name, MY_PARAM_MAX); \
  } while (0)
#define SYSFS_SERIAL(r, de, tgt, name, ifnum, type)                                  \
  do {                                                                               \
    read_sysfs_file_serial(r, de, ifnum, tgt->name, tgt->bConfigurationValue, type); \
  } while (0)
#define SYSFS_CDCACM(r, de, tgt, name, ifnum, type)                                  \
  do {                                                                               \
    read_sysfs_file_cdcacm(r, de, ifnum, tgt->name, tgt->bConfigurationValue, type); \
  } while (0)
#define SYSFS_CDCWDM(r, de, tgt, name, ifnum, type)                                  \
  do {                                                                               \
    read_sysfs_file_cdcwdm(r, de, ifnum, tgt->name, tgt->bConfigurationValue, type); \
  } while (0)
#define SYSFS_USBNET(r, de, tgt, name, ifnum, type)                                  \
  do {                                                                               \
    read_sysfs_file_usbnet(r, de, ifnum, tgt->name, tgt->bConfigurationValue, type); \
  } while (0)

LIST_HEAD(usbsyslist);
LIST_HEAD(usbdevlist);

static const char sys_bus_usb_devices[] = "/sys/bus/usb/devices";

static unsigned int read_sysfs_file_int(const char *d_name, const char *file,
                                        int base) {
  char buf[12], path[MY_PATH_MAX];
  int fd;
  ssize_t r;
  unsigned long ret;
  snprintf(path, MY_PATH_MAX, "%s/%s", d_name, file);
  path[MY_PATH_MAX - 1] = '\0';
  fd = open(path, O_RDONLY);
  if (fd < 0) goto error;
  memset(buf, 0, sizeof(buf));
  r = read(fd, buf, sizeof(buf) - 1);
  close(fd);
  if (r < 0) goto error;
  buf[sizeof(buf) - 1] = '\0';
  ret = strtoul(buf, NULL, base);
  return (unsigned int)ret;

error:
  perror(path);
  return 0;
}

static void read_sysfs_file_string(const char *d_name, const char *file,
                                   char *buf, int len) {
  char path[MY_PATH_MAX];
  int fd;
  ssize_t r;
  fd = snprintf(path, MY_PATH_MAX, "%s/%s", d_name, file);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  fd = open(path, O_RDONLY);
  if (fd < 0) goto error;
  r = read(fd, buf, len);
  close(fd);
  if (r > 0 && r < len) {
    buf[r] = '\0';
    r--;
    while (buf[r] == '\n') {
      buf[r] = '\0';
      r--;
    }
    while (r) {
      if (buf[r] == '\n') buf[r] = ' ';
      r--;
    }
    return;
  }
error:
  buf[0] = '\0';
}

static void read_sysfs_file_serial(const char *root, const char *d_name,
                                   unsigned int ifnum, struct interface *intf, unsigned int cfg, unsigned int type) {
  DIR *dp;
  struct dirent *dirp;
  char buf[MY_PATH_MAX];
  char path[MY_PATH_MAX];
  int fd;

  // Get driver's name
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/driver", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  if (realpath(path, buf) != NULL)
    sprintf((intf + ifnum)->driver, "%s", basename(buf));

  // Get the serial port's devname
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  if ((dp = opendir(path)) == NULL) {
    printf("error: Can't open %s", path);
    goto error;
  }
  while ((dirp = readdir(dp)) != NULL) {
    if (0 == strncmp(dirp->d_name, "ttyUSB", 6)) {
      (intf + ifnum)->index = ifnum;
      (intf + ifnum)->type = type;
      strcpy((intf + ifnum)->name, "/dev/");
      strcat((intf + ifnum)->name, dirp->d_name);
      return;
    }
  }
error:
  (intf + ifnum)->name[0] = '\0';
}

static void read_sysfs_file_cdcacm(const char *root, const char *d_name,
                                   unsigned int ifnum, struct interface *intf, unsigned int cfg, unsigned int type) {
  DIR *dp;
  struct dirent *dirp;
  char buf[MY_PATH_MAX];
  char path[MY_PATH_MAX];
  int fd;

  // Get driver's name
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/driver", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  if (realpath(path, buf) != NULL)
    sprintf((intf + ifnum)->driver, "%s", basename(buf));

  // Get the serial port's devname
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/tty", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  if ((dp = opendir(path)) == NULL) {
    printf("error: Can't open %s", path);
    goto error;
  }
  while ((dirp = readdir(dp)) != NULL) {
    if (0 == strncmp(dirp->d_name, "ttyACM", 6)) {
      (intf + ifnum)->index = ifnum;
      (intf + ifnum)->type = type;
      strcpy((intf + ifnum)->name, "/dev/");
      strcat((intf + ifnum)->name, dirp->d_name);
      return;
    }
  }
error:
  (intf + ifnum)->name[0] = '\0';
}

static void read_sysfs_file_cdcwdm(const char *root, const char *d_name,
                                   unsigned int ifnum, struct interface *intf, unsigned int cfg, unsigned int type) {
  DIR *dp;
  struct dirent *dirp;
  char buf[MY_PATH_MAX];
  char path[MY_PATH_MAX];
  int fd;

  // Get driver's name
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/driver", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  if (realpath(path, buf) != NULL) {
    sprintf((intf + ifnum)->driver, "%s", basename(buf));
    (intf + ifnum)->type = (0 == strcmp("qmi_wwan", (intf + ifnum)->driver)) ? MM_MODEM_PORT_TYPE_QMI : MM_MODEM_PORT_TYPE_MBIM;
  } else
    (intf + ifnum)->type = MM_MODEM_PORT_TYPE_UNKNOWN;

  // Get the network interface name
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/net", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  if ((dp = opendir(path)) == NULL) {
    printf("error: Can't open %s", path);
    goto error;
  }
  while ((dirp = readdir(dp)) != NULL) {
    if ((0 == strcmp(dirp->d_name, ".")) || (0 == strcmp(dirp->d_name, "..")))
      continue;
    strcpy((intf + ifnum)->network, dirp->d_name);
  }
  closedir(dp);

  // Get the cdc-wdm's devname
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/usbmisc", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  if ((dp = opendir(path)) == NULL) {
    printf("error: Can't open %s", path);
    goto error;
  }
  while ((dirp = readdir(dp)) != NULL) {
    if (0 == strncmp(dirp->d_name, "cdc-wdm", 7)) {
      (intf + ifnum)->index = ifnum;
      strcpy((intf + ifnum)->name, "/dev/");
      strcat((intf + ifnum)->name, dirp->d_name);
      return;
    }
  }
error:
  (intf + ifnum)->name[0] = '\0';
  (intf + ifnum)->network[0] = '\0';
}

static void read_sysfs_file_usbnet(const char *root, const char *d_name,
                                   unsigned int ifnum, struct interface *intf, unsigned int cfg, unsigned int type) {
  DIR *dp;
  struct dirent *dirp;
  char buf[MY_PATH_MAX];
  char path[MY_PATH_MAX];
  int fd;

  // Get driver's name
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/driver", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  if (realpath(path, buf) != NULL)
    sprintf((intf + ifnum)->driver, "%s", basename(buf));
  else
    (intf + ifnum)->type = MM_MODEM_PORT_TYPE_UNKNOWN;

  // Get the network interface name
  fd = snprintf(path, MY_PATH_MAX, "%s/%s:%d.%d/net", root, d_name, cfg, ifnum);
  if (fd < 0 || fd >= MY_PATH_MAX) goto error;
  path[fd] = '\0';
  if ((dp = opendir(path)) == NULL) {
    printf("error: Can't open %s", path);
    goto error;
  }
  while ((dirp = readdir(dp)) != NULL) {
    if ((0 == strcmp(dirp->d_name, ".")) || (0 == strcmp(dirp->d_name, "..")))
      continue;
    (intf + ifnum)->index = ifnum;
    if (0 == strcmp("cdc_ether", (intf + ifnum)->driver))
      (intf + ifnum)->type = MM_MODEM_PORT_TYPE_ECM;
    else if (0 == strcmp("cdc_ncm", (intf + ifnum)->driver))
      (intf + ifnum)->type = MM_MODEM_PORT_TYPE_NCM;
    else
      (intf + ifnum)->type = MM_MODEM_PORT_TYPE_NET;
    strcpy((intf + ifnum)->name, dirp->d_name);
    strcpy((intf + ifnum)->network, dirp->d_name);
    return;
  }
error:
  (intf + ifnum)->network[0] = '\0';
}

static void add_usb_device(const char *root, const char *d_name) {
  struct usbdevice *d;

  d = malloc(sizeof(struct usbdevice));
  if (!d) return;
  memset(d, 0, sizeof(struct usbdevice));
  if (snprintf(d->name, MY_SYSFS_FILENAME_LEN, "%s", d_name) >=
      MY_SYSFS_FILENAME_LEN)
    printf("warning: '%s' truncated to '%s'", d_name, d->name);
  SYSFS_INTx(root, d, idVendor);
  SYSFS_INTx(root, d, idProduct);
  SYSFS_INTx(root, d, bNumInterfaces);
  SYSFS_INTu(root, d, bNumConfigurations);
  SYSFS_INTu(root, d, bConfigurationValue);
  d->antennas = 2;

  if (d->idVendor == 0x1bc7) {
    if ((d->idProduct == 0x1900) && (d->bNumInterfaces == 1)) {
      passing_fastenum = 1;
      free(d);
      return;
    }
    if ((d->idProduct == 0x1900) && (d->bConfigurationValue != 1)) {
      passing_fastenum = 1;
      free(d);
      return;
    }

    d->index = usbdevice_count;
    usbdevice_count++;
    SYSFS_STR(root, d, manufacturer);
    SYSFS_STR(root, d, product);
    SYSFS_STR(root, d, serial);
    SYSFS_STR(root, d, version);
    SYSFS_STR(root, d, speed);
    d->intf = malloc(d->bNumInterfaces * sizeof(struct interface));
    memset(d->intf, 0, d->bNumInterfaces * sizeof(struct interface));

    switch (d->idProduct) {
      case 0x0032:  // #USBCFG=3, ACM*6+MBIM
        d->type = TELIT_LE910Bx_FMAILY;
        SYSFS_CDCACM(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_ATPPP);
        SYSFS_CDCACM(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_TRACE);
        SYSFS_CDCACM(root, d_name, d, intf, 6, MM_MODEM_PORT_TYPE_AT);
        SYSFS_CDCWDM(root, d_name, d, intf, 12, MM_MODEM_PORT_TYPE_CDCWDM);
        break;
      case 0x0036:  // #USBCFG=0, ACM*6+NCM
        d->type = TELIT_LE910Bx_FMAILY;
        SYSFS_CDCACM(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_ATPPP);
        SYSFS_CDCACM(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_TRACE);
        SYSFS_CDCACM(root, d_name, d, intf, 6, MM_MODEM_PORT_TYPE_AT);
        SYSFS_USBNET(root, d_name, d, intf, 12, MM_MODEM_PORT_TYPE_NCM);
        break;
      case 0x1040:  // #USBCFG=1, DIAG+ADB+RmNet+NMEA+MODEM+MODEM+AUX
      case 0x1050:  // #USBCFG=1, DIAG+ADB+RmNet+NMEA+MODEM+MODEM+AUX
      case 0x1201:  // #USBCFG=0, DIAG+ADB+RmNet+NMEA+MODEM+MODEM+SAP
      case 0x1260:  // #USBCFG=13, DIAG+ADB+RmNet+NMEA+MODEM+MODEM+SAP
      case 0x1261:  // #USBCFG=14, DIAG+ADB+RmNet+NMEA+MODEM+MODEM+SAP
        if (d->idProduct == 0x1040) {
          if (strstr("LM960A18", d->product) != NULL)
            d->type = TELIT_LM960A18_FMAILY;
          else
            d->type = TELIT_LM940A11_FAMILY;
        } else if (d->idProduct == 0x1050)
          d->type = TELIT_FN980_FAMILY;
        else
          d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_DIAG);
        SYSFS_CDCWDM(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_CDCWDM);
        SYSFS_SERIAL(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 4, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 5, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1042:  // #SUBCFG=0, RNDIS+DIAG+ADB+NMEA+MODEM+MODEM+AUX
      case 0x1052:  // #SUBCFG=0, RNDIS+DIAG+ADB+NMEA+MODEM+MODEM+AUX
      case 0x1203:  // #USBCFG=1, RNDIS+DIAG+ADB+NMEA+MODEM+MODEM+SAP
        if (d->idProduct == 0x1042) {
          if (strstr("LM960A18", d->product) != NULL)
            d->type = TELIT_LM960A18_FMAILY;
          else
            d->type = TELIT_LM940A11_FAMILY;
        } else if (d->idProduct == 0x1052)
          d->type = TELIT_FN980_FAMILY;
        else
          d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_DIAG);
        SYSFS_SERIAL(root, d_name, d, intf, 4, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 5, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 6, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1041:  // #SUBCFG=2, DIAG+ADB+MBIM+NMEA+MODEM+MODEM+AUX
      case 0x1043:  // #SUBCFG=3, DIAG+ADB+ECM+NMEA+MODEM+MODEM+AUX
      case 0x1051:  // #SUBCFG=2, DIAG+ADB+MBIM+NMEA+MODEM+MODEM+AUX
      case 0x1053:  // #SUBCFG=3, DIAG+ADB+ECM+NMEA+MODEM+MODEM+AUX
      case 0x1204:  // #USBCFG=2, DIAG+ADB+MBIM+NMEA+MODEM+MODEM+SAP
      case 0x1206:  // #USBCFG=4, DIAG+ADB+ECM+NMEA+MODEM+MODEM+SAP
        if ((d->idProduct == 0x1041) || (d->idProduct == 0x1043)) {
          if (strstr("LM960A18", d->product) != NULL)
            d->type = TELIT_LM960A18_FMAILY;
          else
            d->type = TELIT_LM940A11_FAMILY;
        } else if ((d->idProduct == 0x1041) || (d->idProduct == 0x1043))
          d->type = TELIT_FN980_FAMILY;
        else
          d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_DIAG);
        if ((d->idProduct == 0x1041) || (d->idProduct == 0x1204))
          SYSFS_CDCWDM(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_CDCWDM);
        SYSFS_SERIAL(root, d_name, d, intf, 4, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 5, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 6, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1250:  // #USBCFG=5, RmNet+NMEA+MODEM+MODEM+SAP
        d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_CDCWDM(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_CDCWDM);
        SYSFS_SERIAL(root, d_name, d, intf, 1, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1251:  // #USBCFG=6, RNDIS+NMEA+MODEM+MODEM+SAP
      case 0x1252:  // #USBCFG=7, MBIM+NMEA+MODEM+MODEM+SAP
      case 0x1253:  // #USBCFG=8, ECM+NMEA+MODEM+MODEM+SAP
        d->type = TELIT_LE910Cx_FAMILY;
        if (d->idProduct == 0x1252)
          SYSFS_CDCWDM(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_CDCWDM);
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 4, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1254:  // #USBCFG=9, MODEM+MODEM
        d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 1, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1255:  // #USBCFG=10, NMEA+MODEM+MODEM+SAP
        d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 1, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1230:  // #USBCFG=11, DIAG+ADB+RmNet+AUDIO+NMEA+MODEM+MODEM+SAP
        d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_DIAG);
        SYSFS_CDCWDM(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_CDCWDM);
        SYSFS_SERIAL(root, d_name, d, intf, 6, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 7, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 8, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1231:  // #USBCFG=12, RNDIS+DIAG+ADB+AUDIO+NMEA+MODEM+MODEM+SAP
        d->type = TELIT_LE910Cx_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_DIAG);
        SYSFS_SERIAL(root, d_name, d, intf, 7, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 8, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 9, MM_MODEM_PORT_TYPE_ATPPP);
        break;
      case 0x1900:  // +USBSWITCH=0
        if ((d->bNumConfigurations == 1) ||
            ((d->bNumConfigurations == 3) && (d->bConfigurationValue == 1))) {
          d->type = TELIT_LN940A9_FAMILY;
          SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_DIAG);
          SYSFS_CDCWDM(root, d_name, d, intf, 1, MM_MODEM_PORT_TYPE_CDCWDM);
          SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_AT);
          SYSFS_SERIAL(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_ATPPP);
          SYSFS_SERIAL(root, d_name, d, intf, 4, MM_MODEM_PORT_TYPE_GPS);
          passing_fastenum = 0;
        }
        break;
      case 0x1901:  // +USBSWITCH=1:
        d->type = TELIT_LN940A9_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_DIAG);
        SYSFS_SERIAL(root, d_name, d, intf, 1, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_ATPPP);
        SYSFS_SERIAL(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_CDCWDM(root, d_name, d, intf, 4, MM_MODEM_PORT_TYPE_CDCWDM);
        break;
      case 0x1910:  // +USBSWITCH=0:
                    //   Cfg=1,RmNet+MODEM+MODEM+NMEA+DIAG
                    //   Cfg=2,MBIM+MODEM+MODEM+NMEA+DIAG
      case 0x1911:  // +USBSWITCH=1:
                    //   MBIM+MODEM+MODEM+NMEA+DIAG
        d->type = TELIT_LN960A16_FAMILY;
        d->antennas = 4;
        SYSFS_CDCWDM(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_CDCWDM);
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_ATPPP);
        SYSFS_SERIAL(root, d_name, d, intf, 4, MM_MODEM_PORT_TYPE_GPS);
        SYSFS_SERIAL(root, d_name, d, intf, 5, MM_MODEM_PORT_TYPE_DIAG);
        break;
      case 0x1101:  // #USBCFG=0: DIAG+MODEM+MODEM+RmNet
      case 0x110a:  // #USBCFG=0: DIAG+MODEM+MODEM+RmNet
        if (d->idProduct == 0x1101)
          d->type = TELIT_ME910C1_FAMILY;
        else
          d->type = TELIT_MEx10G1_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_DIAG);
        SYSFS_SERIAL(root, d_name, d, intf, 1, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_ATPPP);
        SYSFS_CDCWDM(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_CDCWDM);
        break;
      case 0x1102:  // #USBCFG=3: DIAG+MODEM+MODEM+ECM
      case 0x110b:  // #USBCFG=3: DIAG+MODEM+MODEM+ECM
        if (d->idProduct == 0x1101)
          d->type = TELIT_ME910C1_FAMILY;
        else
          d->type = TELIT_MEx10G1_FAMILY;
        SYSFS_SERIAL(root, d_name, d, intf, 0, MM_MODEM_PORT_TYPE_DIAG);
        SYSFS_SERIAL(root, d_name, d, intf, 1, MM_MODEM_PORT_TYPE_AT);
        SYSFS_SERIAL(root, d_name, d, intf, 2, MM_MODEM_PORT_TYPE_ATPPP);
        SYSFS_USBNET(root, d_name, d, intf, 3, MM_MODEM_PORT_TYPE_ECM);
        break;
    }
    list_add_tail(&d->list, &usbdevlist);
  } else
    free(d);
}

static void inspect_bus_entry(const char *d_name) {
  char buf[MY_PATH_MAX];
  char path[MY_PATH_MAX];
  int n;

  if (d_name[0] == '.' && (!d_name[1] || (d_name[1] == '.' && !d_name[2])))
    return;
  if (d_name[0] == 'u' && d_name[1] == 's' && d_name[2] == 'b' &&
      isdigit(d_name[3])) {
    // add_usb_bus(d_name);
  } else if (isdigit(d_name[0])) {
    // if (strchr(d_name, ':') == NULL) add_usb_device(d_name);
    if (strchr(d_name, ':') == NULL) {
      n = snprintf(path, MY_PATH_MAX, "%s/%s", sys_bus_usb_devices, d_name);
      if (n > 0 && n < MY_PATH_MAX) {
        if (realpath(path, buf) != NULL) {
          struct usbsysfs *s;

          s = malloc(sizeof(struct usbsysfs));
          if (!s) return;
          memset(s, 0, sizeof(struct usbsysfs));
          strcpy(s->syspath, buf);
          strcpy(s->d_name, d_name);
          list_add_tail(&s->list, &usbsyslist);
          //ALOGD("[Brian] %s", buf);
        }
      }
    }
  } else
    fprintf(stderr, "ignoring '%s'", d_name);
}

static const char *formatModuleStr(unsigned int idx, struct interface intf) {
  static char buf[MY_PATH_MAX];

  switch (intf.type) {
    case MM_MODEM_PORT_TYPE_UNKNOWN:
      buf[0] = '\0';
      break;
    case MM_MODEM_PORT_TYPE_AT:
      sprintf(buf, "|__ If %d, %s (%s), Driver=%s, at='primary'", idx, intf.name,
              MMModemPortType_String[intf.type], intf.driver);
      break;
    case MM_MODEM_PORT_TYPE_ATPPP:
      sprintf(buf, "|__ If %d, %s (%s), Driver=%s, at='secondary'", idx, intf.name,
              MMModemPortType_String[intf.type], intf.driver);
      break;
    case MM_MODEM_PORT_TYPE_QMI:
    case MM_MODEM_PORT_TYPE_MBIM:
      sprintf(buf, "|__ If %d, %s (%s), Driver=%s, net='%s'", idx, intf.name,
              MMModemPortType_String[intf.type], intf.driver, intf.network);
      break;
    default:
      sprintf(buf, "|__ If %d, %s (%s), Driver=%s", idx, intf.name,
              MMModemPortType_String[intf.type], intf.driver);
      break;
  }

  return buf;
}

static void walk_usb_devices(DIR *sbud) {
  struct dirent *de;
  while ((de = readdir(sbud))) {
    inspect_bus_entry(de->d_name);
  }
  closedir(sbud);

  struct list_head *l;
  struct usbsysfs *fs;
  for (l = usbsyslist.next; l != &usbsyslist; l = l->next) {
    fs = list_entry(l, struct usbsysfs, list);
    add_usb_device(fs->syspath, fs->d_name);
    //ALOGD("[BRIAN] %s", fs->syspath);
  }
}

static void print_tree(void) {
  unsigned int i;
  struct list_head *l;
  struct usbdevice *pd;
  const char *data;

  for (l = usbdevlist.next; l != &usbdevlist; l = l->next) {
    pd = list_entry(l, struct usbdevice, list);

    ALOGD("Module=%s Vendor=%04x ProdID=%04x Cfg#=%d/%d",
          MMBroadbandModemTelit[pd->type].value_name, pd->idVendor, pd->idProduct,
          pd->bConfigurationValue, pd->bNumConfigurations);
    for (i = 0; i < pd->bNumInterfaces; i++) {
      data = formatModuleStr(i, *(pd->intf + i));
      if (strcmp(data, ""))
        ALOGD("   %s", data);
    }
  }
}

void print_usb_devices(void) {
  unsigned int i;
  struct list_head *l;
  struct usbdevice *pd;
  const char *data;

  for (l = usbdevlist.next; l != &usbdevlist; l = l->next) {
    pd = list_entry(l, struct usbdevice, list);

    printf("Id=%d, Module=%s Vendor=%04x ProdID=%04x Cfg#=%d/%d\n", pd->index,
           MMBroadbandModemTelit[pd->type].value_name, pd->idVendor, pd->idProduct,
           pd->bConfigurationValue, pd->bNumConfigurations);
    for (i = 0; i < pd->bNumInterfaces; i++) {
      data = formatModuleStr(i, *(pd->intf + i));
      if (strcmp(data, ""))
        printf("   %s\n", data);
    }
  }
}

char *get_module_description(void) {
  char *buff = (char *)malloc(MY_SYSFS_FILENAME_LEN);

  sprintf(buff, "Product: %s\nManufacturer: Telit\nSerialNumber: %s\n",
          s_active_module->product, s_active_module->serial);
  return buff;
}

char *get_module_product(void) {
  char *buff = (char *)malloc(MY_SYSFS_FILENAME_LEN);

  switch (s_active_module->type) {
    case TELIT_LN940A9_FAMILY:
    case TELIT_LN960A16_FAMILY:
      strcpy(buff, MMBroadbandModemTelit[s_active_module->type].value_name);
      break;
    default:
      strcpy(buff, s_active_module->product);
      break;
  }

  return buff;
}

MMBoardbandModem_Telit get_module_type(void) {
  return s_active_module->type;
}

void get_atcmd_port(char *buf, unsigned int m, unsigned int p) {
  unsigned int i = 0, j;
  struct list_head *l;
  struct usbdevice *pd;

  buf[0] = '\0';
  for (l = usbdevlist.next; l != &usbdevlist; l = l->next) {
    if (i == m) {
      pd = list_entry(l, struct usbdevice, list);

      for (j = 0; j < pd->bNumInterfaces; j++) {
        struct interface intf = *(pd->intf + j);
        if (p == 1) {
          if (intf.type == MM_MODEM_PORT_TYPE_ATPPP) {
            strcpy(buf, intf.name);
            s_active_module = pd;
            break;
          }
        } else {
          if (intf.type == MM_MODEM_PORT_TYPE_AT) {
            strcpy(buf, intf.name);
            s_active_module = pd;
            break;
          }
        }
      }
    }
    i++;
  }
}

int get_bootup_mode(void) { return is_first_bootup; }

int lsusb_telit(void) {
  DIR *sbud = opendir(sys_bus_usb_devices);
  if (sbud) {
    walk_usb_devices(sbud);
    print_tree();
    return usbdevice_count;
  } else
    perror(sys_bus_usb_devices);
  return sbud == NULL;
}
