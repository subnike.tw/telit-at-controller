#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <libgen.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

// C Custom Header File
#include "atchannel.h"
#include "log.h"
#include "lsusb-telit.h"
#include "misc.h"
#include "reference-ril.h"

static char s_ATCmdPort[16] = {'\0'};

static const char *at_command = "AT";
static int list_flag = 0;
static int module_index = 0;
static int macro_index = 0;
static int port_index = 0;
static int retry_count = 10;
static int timeout_response = 0;
static int verbose_flag = 0;
static bool rxlevel_lte = true;
static int rxlevel_band = 0;
static int display_flag = 0;

static int module_count = 0;

#define MARCO_CUSTOM_ATCMD 0x0100
#define MACRO_DISABLE_WCDMA 0x0101
#define MACRO_FACROTY_SIM_TESTING 0x0102
#define MACRO_READ_NETWORK_STATUS 0x0103
#define MACRO_READ_RX_POWER_LEVEL 0x0104

static void print_usage_and_exit(const char *prog) {
  printf("Usage: %s [-ahilmptvV]\n", basename((char *)prog));
  puts(
      "  -a --atcmd    AT Command string send to module\n"
      "  -d --display  display the module description\n"
      "  -h --help     display the usage\n"
      "  -l --list     list all available modules\n"
      "  -m --module   the index of the modules\n"
      "  -p --port     the index of AT Command port\n"
      "  -r --retry    retry times (default = 10 times per second)\n"
      "  -t --timeout  timeout for AT Command response (ms)\n"
      "  -v --verbose  enable verbose output\n"
      "  -V --version  print version\n"
      "\n"
      "  --macro-disable-wcdma        disable WCDMA function\n"
      "  --macro-factory-sim-testing  SIM factory testing\n"
      "  --macro-read-network-status  diagnostic the module\n"
      "  --macro-read-rx-power-level  read RX level (optional: [LTE band only])\n");
  exit(EXIT_SUCCESS);
}

static void print_version_and_exit(const char *prog) {
  printf("\n%s %s\n", basename((char *)prog), VERSION_NUMBER);
  puts(
      "Copyright (C) 2019-2020 Brian Lee\n"
      "License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl-2.0.html>\n"
      "This is free software: you are free to change and redistribute it.\n"
      "There is NO WARRANTY, to the extent permitted by law.\n");
  exit(EXIT_SUCCESS);
}

static void parse_opts(int argc, char *argv[]) {
  static const struct option lopts[] = {
      {"atcmd", required_argument, 0, 'a'},
      {"display", no_argument, 0, 'd'},
      {"help", no_argument, 0, 'h'},
      {"list", no_argument, 0, 'l'},
      {"module", required_argument, 0, 'm'},
      {"port", required_argument, 0, 'p'},
      {"retry", required_argument, 0, 'r'},
      {"timeout", required_argument, 0, 't'},
      {"verbose", no_argument, 0, 'v'},
      {"version", no_argument, 0, 'V'},

      {"marco-disable-wcdma", no_argument, 0, MACRO_DISABLE_WCDMA},
      {"macro-disable-wcdma", no_argument, 0, MACRO_DISABLE_WCDMA},
      {"macro-factory-sim-testing", no_argument, 0, MACRO_FACROTY_SIM_TESTING},
      {"macro-read-network-status", no_argument, 0, MACRO_READ_NETWORK_STATUS},
      {"macro-read-rx-power-level", required_argument, 0, MACRO_READ_RX_POWER_LEVEL},
      {NULL, 0, 0, 0},
  };

  for (;;) {
    /* getopt_long stores the option index here. */
    int c = getopt_long(argc, argv, "a:dhi:lm:p:r:t:vV", lopts, NULL);

    if (c == -1) break;

    switch (c) {
      case 'a':
        macro_index = MARCO_CUSTOM_ATCMD;
        at_command = optarg;
        break;
      case 'd':
        display_flag = 1;
        break;
      case 'h':
        print_usage_and_exit(argv[0]);
        break;
      case 'l':
        list_flag = 1;
        break;
      case 'm':
        module_index = atoi(optarg);
        break;
      case 'p':
        port_index = atoi(optarg);
        break;
      case 'r':
        retry_count = atoi(optarg);
        break;
      case 't':
        timeout_response = atoi(optarg);
        //if (timeout_response > 0) at_set_timeout(timeout_response);
        break;
      case 'v':
        verbose_flag = 1;
        set_log_level(LOG_LEVEL_VERBOSE);
        set_log_timestamp(true);
        break;
      case 'V':
        print_version_and_exit(argv[0]);
        break;
      case MACRO_DISABLE_WCDMA:
      case MACRO_FACROTY_SIM_TESTING:
      case MACRO_READ_NETWORK_STATUS:
      case MACRO_READ_RX_POWER_LEVEL:
        if (c == MACRO_READ_RX_POWER_LEVEL) {
          char *pch = strchr(optarg, ',');
          if (pch != NULL) {
            if (0 == strncmp(optarg, "WCDMA,", 6)) {
              rxlevel_lte = false;
              rxlevel_band = atoi(++pch);
            }
            print_usage_and_exit(argv[0]);
          } else
            rxlevel_band = atoi(optarg);
        }
        macro_index = c;
        break;
      default:
        print_usage_and_exit(argv[0]);
        break;
    }
  }
}

int main(int argc, char *argv[]) {
  int ret = EXIT_SUCCESS;
  struct timespec tstart = {0, 0}, tend = {0, 0};

  parse_opts(argc, argv);

  clock_gettime(CLOCK_MONOTONIC, &tstart);
  module_count = lsusb_telit();
  clock_gettime(CLOCK_MONOTONIC, &tend);
  ALOGD("Spend %.5f seconds to 'Find ATPort'",
        ((double)tend.tv_sec + 1.0e-9 * tend.tv_nsec) -
            ((double)tstart.tv_sec + 1.0e-9 * tstart.tv_nsec));

  if (list_flag) {
    print_usb_devices();
    exit(EXIT_SUCCESS);
  }

  if (module_count > 0) {
    if (module_index >= module_count) {
      ALOGI("Using the first module");
      module_index = 0;
    }
    get_atcmd_port(s_ATCmdPort, module_index, port_index);
    ALOGD("Using port '%s'", s_ATCmdPort);

    if (display_flag)
      printf("%s\n", get_module_description());
  }

  if (strcmp(s_ATCmdPort, "") == 0) {
    printf("No module found on system\n");
    exit(EXIT_FAILURE);
  }

  int fd = -1;
  while (fd < 0) {
#if 0
    if (access(s_ATCmdPort, R_OK | W_OK) != 0) {
      printf("Fail to access %s, errno: %d (%s)\n", s_ATCmdPort, errno,
             strerror(errno));
      usleep(100000);
      continue;
    }
#endif
    fd = open(s_ATCmdPort, O_RDWR);

    if (fd >= 0 && !memcmp(s_ATCmdPort, "/dev/tty", 8)) {
      if (ioctl(fd, TIOCEXCL) == -1) {
        perror("error ioctl:");
        exit(-1);
      }
      /* disable echo on serial ports */
      struct termios ios;
      tcgetattr(fd, &ios);
      ios.c_lflag = 0; /* disable ECHO, ICANON, etc... */
      tcsetattr(fd, TCSANOW, &ios);
    }

    if (fd < 0) {
      perror("Failed to open AT port, reason");
      exit(EXIT_FAILURE);
    }
  }

  ret = at_open(fd);

  at_handshake();

  if (ret < 0) {
    ALOGE("AT error %d on at_open", ret);
    exit(EXIT_FAILURE);
  }

  clock_gettime(CLOCK_MONOTONIC, &tstart);
  switch (macro_index) {
    case MARCO_CUSTOM_ATCMD:
      ret = requestCustomATCmd(at_command);
      break;
    case MACRO_DISABLE_WCDMA:
      ret = requestDisableWCDMA();
      break;
    case MACRO_FACROTY_SIM_TESTING:
      ret = requestFactorySimTesting();
      break;
    case MACRO_READ_NETWORK_STATUS:
      ret = requestReadNetworkStatus();
      break;
    case MACRO_READ_RX_POWER_LEVEL:
      ret = requestReadRXPowerLevel(rxlevel_lte, rxlevel_band);
      break;
    default:
      print_usage_and_exit(argv[0]);
      break;
  }
  clock_gettime(CLOCK_MONOTONIC, &tend);

  at_close();
  close(fd);

  ALOGD("Spend %.5f seconds to 'Send ATCmd'",
        ((double)tend.tv_sec + 1.0e-9 * tend.tv_nsec) -
            ((double)tstart.tv_sec + 1.0e-9 * tstart.tv_nsec));

  return ret;
}
