#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <syslog.h>
#include <time.h>

// C Custom Header File
#include "log.h"

#define MAX_MSG_LEN 32

int log_level = LOG_LEVEL_INFO;
bool log_to_syslog = false;
bool log_timestamp = false;

void set_log_level(int level) { log_level = level; }

void set_syslog(bool enable) { log_to_syslog = enable; }

void set_log_timestamp(bool enable) { log_timestamp = enable; }

static const char *get_now(void) {
  static char buff[50];
  struct timeval tv;
  time_t time;
  suseconds_t millitm;
  struct tm *ti;

  gettimeofday(&tv, NULL);

  time = tv.tv_sec;
  millitm = (tv.tv_usec + 500) / 1000;

  if (millitm == 1000) {
    ++time;
    millitm = 0;
  }

  ti = localtime(&time);
  sprintf(buff, "%04d/%02d/%02d %02d:%02d:%02d.%03d", ti->tm_year + 1900,
          ti->tm_mon + 1, ti->tm_mday, ti->tm_hour, ti->tm_min, ti->tm_sec,
          (int)millitm);
  return buff;
}

void dbg_print(int level, const char *fmt, ...) {
  if (log_level < level) return;

  struct {
    char *tag;
    char *short_tag;
    int prio;
    FILE *stream;
  } log[] = {[LOG_LEVEL_ERROR] = {"ERR", "E", LOG_ERR, stderr},
             [LOG_LEVEL_INFO] = {"INF", "I", LOG_INFO, stdout},
             [LOG_LEVEL_VERBOSE] = {"DBG", "D", LOG_DEBUG, stdout}};

  va_list args;
  va_start(args, fmt);

  if (log_to_syslog) {
    openlog("lxfp", LOG_CONS, LOG_USER);
    vsyslog(log[level].prio, fmt, args);
    closelog();
  } else {
    if (log_timestamp)
      fprintf(log[level].stream, "[%s] %s/", get_now(), log[level].short_tag);
    else
      fprintf(log[level].stream, "[%s] ", log[level].tag);
    vfprintf(log[level].stream, fmt, args);
  }

  va_end(args);
}

void log_progress(void) {
#if !defined(ANDROID_CHANGES)
  if (!log_to_syslog) {
    static int progress = 0;
    static char progressBar[] = {'|', '\\', '-', '/'};

    dbg_print(LOG_LEVEL_INFO, "Flashing... %c\r",
              progressBar[progress++ % sizeof(progressBar)]);
  }
#endif
}
