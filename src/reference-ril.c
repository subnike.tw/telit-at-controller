#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// C Custom Header File
#include "at_tok.h"
#include "atchannel.h"
#include "frequency_band.h"
#include "log.h"
#include "lsusb-telit.h"
#include "qmi-enum-types.h"
#include "reference-ril.h"

#define NUM_ELEMS(x) (sizeof(x) / sizeof((x)[0]))

static int s_pci = 0;

RIL_Errno requestCustomATCmd(const char *cmd) {
  int err;
  ATResponse *p_response = NULL;

  if (0 == strcmp(cmd, "AT#BND?"))
    err = requestBandCapability();
  else if (0 == strcmp(cmd, "AT#RFSTS")) {
    if ((TELIT_LM940A11_FAMILY == get_module_type()) || (TELIT_LM960A18_FMAILY == get_module_type()))
      err = requestReadCurrentNetworkStatus(true);
    else
      err = requestReadCurrentNetworkStatus(false);
  } else if (0 == strcmp(cmd, "AT#CAINFO?"))
    err = requestShowCAInformation();
  else {
    err = at_send_command(cmd, &p_response);
    if (err < 0 || p_response->success == 0) goto error;
  }

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

#define LTE_BAND_CAPABILITY_BAND66 0x0008000000000000
#define LTE_BAND_CAPABILITY_BAND71 0x0800000000000000

void outputBandCapabilityString(const char *prefix, u_int8_t n, u_int16_t *bnd) {
  u_int8_t i;

  for (i = 0; i < n; i++) {
    if (i > 0)
      printf(",%d", *(bnd + i));
    else
      printf("\n%s: %d", prefix, *(bnd + i));
  }
}

void getBandCapability(long value, u_int16_t base, u_int8_t *num, u_int16_t *bnd) {
  u_int8_t n = *num;
  long mask;

  for (u_int8_t i = 0; i < 48; i++) {
    mask = ((unsigned long)1) << i;
    if ((value & mask) > 0) {
      bnd[n++] = i + base;
    }
  }

  *num = n;
}

RIL_Errno requestBandCapability(void) {
  int err;
  ATResponse *p_response = NULL;

  char *line;
  long bands;
  char *buff = (char *)malloc(sizeof(char) * MY_PARAM_MAX);
  struct BAND_CAPABILITY_T bandCapability;
  memset(&bandCapability, 0, sizeof(struct BAND_CAPABILITY_T));

  err = at_send_command_singleline("AT#BND?", "#BND:", &p_response);
  if (err < 0 || p_response->success == 0) goto error;

  line = p_response->p_intermediates->line;
  err = at_tok_start(&line);
  if (err < 0) goto error;

  printf("===== Band Capability  =====");

  err = at_tok_nextlong(&line, &bands);
  if (err < 0) bands = 0;

  if (bands > 0)
    printf("\nGSM: %s", getGSMBandCapabilityString(bands));

  err = at_tok_nextlong(&line, &bands);
  if (err < 0) bands = 0;

  if (bands > 0)
    printf("\nUMTS: %s", getUMTSBandCapabilityString(bands));

  // #BND: ,,<LTE_band>,<LTE_band_ext>,
  //       <NSA_NR5G_band_1_64>,<NSA_NR5G_band_65_128>,<NSA_NR5G_band_257_320>,
  //       <SA_NR5G_band_1_64>,<SA_NR5G_band_65_128>,<SA_NR5G_band_257_320>
  int count = 0;
  while (at_tok_hasmore(&line)) {
    err = at_tok_nexthexlong(&line, &bands);
    if (err < 0) bands = 0;

    if (bands > 0) {
      memset(buff, 0, MY_PARAM_MAX);

      switch (count) {
        case 0:
          getBandCapability(bands, 1, &bandCapability.numLTE, bandCapability.bndLTE);
          // LE910Cx, special case
          if (TELIT_LE910Cx_FAMILY == get_module_type()) {
            if ((bands & LTE_BAND_CAPABILITY_BAND66) > 0)
              printf(",66");
            if ((bands & LTE_BAND_CAPABILITY_BAND71) > 0)
              printf(",71");
          }

          break;
        case 1:
          getBandCapability(bands, 65, &bandCapability.numLTE, bandCapability.bndLTE);
          break;
        case 2:
          getBandCapability(bands, 1, &bandCapability.numNSA, bandCapability.bndNSA);
          break;
        case 3:
          getBandCapability(bands, 65, &bandCapability.numNSA, bandCapability.bndNSA);
          break;
        case 4:
          getBandCapability(bands, 257, &bandCapability.numNSA, bandCapability.bndNSA);
          break;
        case 5:
          getBandCapability(bands, 1, &bandCapability.numSA, bandCapability.bndSA);
          break;
        case 6:
          getBandCapability(bands, 65, &bandCapability.numSA, bandCapability.bndSA);
          break;
        case 7:
          getBandCapability(bands, 257, &bandCapability.numSA, bandCapability.bndSA);
          break;
        default:
          break;
      }
    }
    count++;
  };

  if (bandCapability.numLTE > 0)
    outputBandCapabilityString("LTE", bandCapability.numLTE, bandCapability.bndLTE);
  if (bandCapability.numNSA > 0)
    outputBandCapabilityString("NR-NSA", bandCapability.numNSA, bandCapability.bndNSA);
  if (bandCapability.numSA > 0)
    outputBandCapabilityString("NR-SA", bandCapability.numSA, bandCapability.bndSA);

  printf("\n============================\n");

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

RIL_Errno requestReadCurrentNetworkStatus(bool isLteA) {
  int err;
  ATResponse *p_response = NULL;

  char *line;
  char *plmn;
  int dl_earfcn = 0;
  int rsrp = 0;
  int rsrq = 0;
  int rssi = 0;
  int tac = 0;
  char *skip;
  int drx = 0;
  int rrc = 0;
  int cid = 0;
  int sd = 0;
  int abnd = 0;
  int sinr = 0;

  err = at_send_command_singleline("AT#RFSTS", "#RFSTS:", &p_response);
  if (err < 0 || p_response->success == 0) goto error;

  line = p_response->p_intermediates->line;
  err = at_tok_start(&line);
  if (err < 0) goto error;

  err = at_tok_nextstr(&line, &plmn);
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &dl_earfcn);
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &rsrp);
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &rssi);
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &rsrq);
  if (err < 0) goto error;

  err = at_tok_nexthexint(&line, &tac);
  if (err < 0) goto error;

  if (isLteA) {
    err = at_tok_nextstr(&line, &skip);  // Skip: RAC, Routing Area Code
    if (err < 0) goto error;
  }

  err = at_tok_nextstr(&line, &skip);  // Skip: TXPWR, Tx Power (In traffic only)
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &drx);
  if (err < 0) goto error;

  err = at_tok_nextstr(&line, &skip);  // Skip: MM, Mobility Management
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &rrc);
  if (err < 0) goto error;

  err = at_tok_nexthexint(&line, &cid);
  if (err < 0) goto error;

  err = at_tok_nextstr(&line, &skip);  // Skip: IMSI, International Mobile Station ID
  if (err < 0) goto error;

  err = at_tok_nextstr(&line, &skip);  // Skip: NetNameAsc, Operation Name
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &sd);
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &abnd);
  if (err < 0) goto error;

  if (!isLteA) {
    err = at_tok_nextint(&line, &sinr);
    if (err < 0) goto error;
  }

  printf("===== Read Network Status =====\n");
  printf("EARFCN(DL/UL): %d/%d\n", dl_earfcn, get_ul_earfcn(dl_earfcn));
  printf("BAND: %d\n", abnd);
  printf("PLMN: %s\n", plmn);
  printf("TAC: %d\n", tac);
  if (s_pci > 0)
    printf("eNB ID(PCI): %d(%d)\n", (cid >> 8), s_pci);
  else
    printf("eNB ID(PCI): %d(-)\n", (cid >> 8));
  printf("DRX: %dms\n", drx);
  printf("RSRP: %ddBm\n", rsrp);
  printf("RSRQ: %ddB\n", rsrq);
  printf("RSSI: %ddBm\n", rssi);
  printf("RRC Status: %s\n", (rrc == 0) ? "IDLE" : "CELL DCH");
  printf("SVC: %s\n", qmi_nas_network_service_domain_get_string(sd));
  printf("===============================\n");

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}
//Get LTE Cphy CA Info
static const struct GEnumValue ca_info_values[] = {
    {0, "BAND", "BAND"},
    {1, "CHANNEL", "CHANNEL"},
    {2, "BW", "BW"},
    {3, "PCI", "PCI"},
    {4, "RSRP", "RSRP"},
    {5, "RSSI", "RSSI"},
    {6, "RSRQ", "RSRQ"},
    {7, "SINR", "SINR"},
    {8, "STATE", "STATE"},
    {9, "MODULATION", "MODULATION"},
    {10, "", ""},
    {0, NULL, NULL}};

RIL_Errno requestShowCAInformation(void) {
  int err;
  ATResponse *p_response = NULL;

  char *line;
  int data, tac;
  bool hasCC = true;

  err = at_send_command_singleline("AT#CAINFO?", "#CAINFO:", &p_response);
  if (err < 0 || p_response->success == 0) goto error;

  line = p_response->p_intermediates->line;
  err = at_tok_start(&line);
  if (err < 0) goto error;

  printf("===== LTE Cphy CA Info =====\n");
  // #CAINFO: <band_class>,<rx_channel>,<dl_bw>,<pci>,<rsrp>,<rssi>,<rsrq>,<sinr>,<tac>,<tx_power>,<uplink_modulation>,<downlink_modulation>,
  //          <band_class>,<rx_channel>,<dl_bw>,<pci>,<rsrp>,<rssi>,<rsrq>,<sinr>,<state>,<uplink_modulation>,<downlink_modulation>,
  //          <band_class>,<rx_channel>,<dl_bw>,<pci>,<rsrp>,<rssi>,<rsrq>,<sinr>,<state>,<uplink_modulation>,<downlink_modulation>,
  //          <band_class>,<rx_channel>,<dl_bw>,<pci>,<rsrp>,<rssi>,<rsrq>,<sinr>,<state>,<uplink_modulation>,<downlink_modulation>,
  //          <band_class>,<rx_channel>,<dl_bw>,<pci>,<rsrp>,<rssi>,<rsrq>,<sinr>,<state>,<uplink_modulation>,<downlink_modulation>
  int count = 0, row = 0, col = 0;
  while (at_tok_hasmore(&line)) {
    col = count % 11;

    if (8 == count) {
      err = at_tok_nexthexint(&line, &tac);
      if (err < 0) tac = 0;
    }

    err = at_tok_nextint(&line, &data);
    if (err < 0) data = 0;

    if (0 == col)
      hasCC = data > 0;

    if (hasCC) {
      switch (col) {
        case 0:
          if (0 == row)
            printf("PCC: %s=%s", ca_info_values[col].value_nick, qmi_nas_active_band_get_string(data));
          else
            printf("SCC%d: %s=%s", row, ca_info_values[col].value_nick, qmi_nas_active_band_get_string(data));
          break;
        case 2:
          printf(", %s=%s MHz", ca_info_values[col].value_nick, qmi_nas_dl_bandwidth_get_string(data));
          break;
        case 8:
          if (0 == row)
            printf(", TAC: %x, TX_PWR=%d", tac, data);
          else
            printf(", %s=%d", ca_info_values[col].value_nick, data);
          break;
        case 9:
          printf(", %s=%s", ca_info_values[col].value_nick, qmi_nas_modulation_get_string(data));
          break;
        case 10:
          printf("/%s", qmi_nas_modulation_get_string(data));
          break;
        default:
          if ((3 == col) && (0 == row))
            s_pci = data;
          printf(", %s=%d", ca_info_values[col].value_nick, data);
          break;
      }
    }

    if (10 == col) {
      if (hasCC)
        printf("\n");

      row++;
      hasCC = false;
    }

    count++;
  }
  printf("============================\n");

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

RIL_Errno requestSelectMode(void) {
  int err;
  ATResponse *p_response = NULL;

  char *line;
  int pref_term;
  int perf_mode;

  err = at_send_command_singleline("AT^SLMODE?", "^SLMODE:", &p_response);
  if (err < 0 || p_response->success == 0) goto error;

  line = p_response->p_intermediates->line;
  err = at_tok_start(&line);
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &pref_term);
  if (err < 0) goto error;

  err = at_tok_nextint(&line, &perf_mode);
  if (err < 0) goto error;

  if (perf_mode != 30) {
    at_response_free(p_response);
    err = at_send_command("AT^SLMODE=1,30", &p_response);
    if (err < 0 || !p_response->success) goto error;
  }

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

RIL_Errno requestSelectBand(void) {
  int err;
  ATResponse *p_response = NULL;

  int i;
  ATLine *p_cur;
  char *tech;

  err = at_send_command_multiline("AT^SLBAND?", "^SLBAND:", &p_response);
  /* we expect 2 lines here:
   * ^SLBAND: WCDMA,1,2,4,5,6,8,19
   * ^SLBAND: LTE,1-5,7,8,12,13,17-21,25,26,28-30,38-41,66
   */

  if (err != 0) goto error;

  for (i = 0, p_cur = p_response->p_intermediates; p_cur != NULL;
       p_cur = p_cur->p_next, i++) {
    char *line = p_cur->line;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextstr(&line, &tech);
    if (err < 0) goto error;

    if (strcmp(tech, "WCDMA") == 0) {
      if (!at_tok_hasmore(&line)) {
        at_response_free(p_response);
        return RIL_E_SUCCESS;
      }
    }
  }

  at_response_free(p_response);
  if (TELIT_LN960A16_FAMILY == get_module_type())
    err = at_send_command("AT^SLBAND=WCDMA,2,", &p_response);
  else
    err = at_send_command("AT^SLBAND=WCDMA,", &p_response);
  if (err < 0 || !p_response->success) goto error;

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

RIL_Errno requestDisableWCDMA(void) {
  int err;
  ATResponse *p_response = NULL;

  err = at_send_command("AT", NULL);
  err = at_send_command("AT", &p_response);
  if (err < 0 || !p_response->success) goto error;

  if (RIL_E_SUCCESS != requestSelectMode()) goto error;
  if (RIL_E_SUCCESS != requestSelectBand()) goto error;

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

/*
 * Function: Get the SIM report for factory testing
 */
RIL_Errno _requestFactorySimTesting(const char *prod, const struct GEnumValue *rows) {
  int err;
  ATResponse *p_response = NULL;

  char *line;
  char *data;
  int i;
  bool isPass = false;

  printf("===== Factory SIM Testing =====\n");
  at_dump_disable(true);
  err = at_send_command("AT+CMEE=2", NULL);

  for (i = 0; rows[i].value_nick; i++) {
    if (0 == rows[i].value) {
      err = at_send_command_numeric(rows[i].value_name, &p_response);
      if (err < 0 || p_response->success == 0) goto error;

      data = p_response->p_intermediates->line;
    } else {
      char *cmd = strdup(rows[i].value_name);
      char *pch = strchr(cmd, '?');
      if (pch)
        *pch = 0;
      char *prefix = strndup(cmd + rows[i].value, strlen(rows[i].value_name));
      strcat(prefix, ":");

      err = at_send_command_singleline(rows[i].value_name, prefix, &p_response);
      if (err < 0 || p_response->success == 0) goto error;

      line = p_response->p_intermediates->line;
      err = at_tok_start(&line);
      if (err < 0) goto error;

      err = at_tok_nextstr(&line, &data);
      if (err < 0) goto error;
    }

    if (0 == i) {
      if (0 == strcmp("READY", data)) {
        isPass = true;
        printf("%s: \033[1;32m%s\033[0m\n", rows[i].value_nick, data);
      } else
        printf("%s: \033[1;31m%s\033[0m\n", rows[i].value_nick, data);
    } else if (1 == i)
      printf("%s: %s\n", rows[i].value_nick, (prod != NULL) ? prod : data);
    else
      printf("%s: %s\n", rows[i].value_nick, data);
  }
  at_dump_disable(false);

  printf("===============================\n");
  if (isPass)
    printf("Result: \033[1;32mPASS\033[0m\n");
  else
    printf("Result: \033[1;31mFAIL\033[0m\n");

  return RIL_E_SUCCESS;

error:
  printf("===============================\n");
  printf("Result: \033[1;31mFAIL\033[0m\n");
  return RIL_E_GENERIC_FAILURE;
}

RIL_Errno requestFactorySimTesting(void) {
  if ((TELIT_LN940A9_FAMILY == get_module_type()) || (TELIT_LN960A16_FAMILY == get_module_type())) {
    const char *prod = get_module_product();
    const struct GEnumValue atcmd_sim_testing[] = {
        {2, "AT+CPIN?", "SIM Status"},
        {0, "AT+GMM", "MODEL"},
        {2, "AT^VERSION?", "FWREV"},
        {0, "AT+GSN", "IMEI"},
        {0, "AT+CIMI", "IMSI"},
        {3, "AT+ICCID", "ICCID"},
        {0, NULL, NULL}};

    return _requestFactorySimTesting(prod, atcmd_sim_testing);
  } else {
    const struct GEnumValue atcmd_sim_testing[] = {
        {2, "AT+CPIN?", "SIM Status"},
        {2, "AT#CGMM", "MODEL"},
        {2, "AT#CGMR", "FWREV"},
        {0, "AT+CGSN", "IMEI"},
        {0, "AT+CIMI", "IMSI"},
        {2, "AT+ICCID", "ICCID"},
        {0, NULL, NULL}};

    return _requestFactorySimTesting(NULL, atcmd_sim_testing);
  }
}

/*
 * Function: Get the network status
 */
RIL_Errno _requestReadNetworkStatusLNSeries(void) {
  ATResponse *p_response = NULL;
  int i;

  char *diagATCmd[] = {
      "AT^VERSION?",
      "AT+GSN",
      "AT+CIMI",
      "AT+ICCID",
      "AT+CPIN?",
      "AT+CREG?",
      "AT+CGREG?",
      "AT+CEREG?",
      "AT+COPS?",
      "AT+CSQ",
      "AT+CGPIAF=1,0,0,0",
      "AT+CGDCONT?",
      "AT$QCPDPP?",
      "AT+CGPADDR",
      "AT+CGCONTRDP",
      "AT^CA_ENABLE?",
      "AT^CA_INFO?",
      "AT^DEBUG?",
  };

  for (i = 0; i < NUM_ELEMS(diagATCmd); i++)
    at_send_command(*(diagATCmd + i), &p_response);

  return RIL_E_SUCCESS;
}

RIL_Errno _requestReadNetworkStatusLegacy(bool isLteA) {
  ATResponse *p_response = NULL;
  int i;

  char *diagATCmd[] = {
      "AT+CGMM",
      "AT+CGMR",
      "AT+CGSN",
      "AT+CIMI",
      "AT+ICCID",
      "AT+CPIN?",
      "AT+CREG?",
      "AT+CGREG?",
      "AT+CEREG?",
      "AT+COPS?",
      "AT+CSQ",
      "AT+CGPIAF=1,0,0,0",
      "AT+CGDCONT?",
      "AT+CGPADDR=",
      "AT+CGCONTRDP=",
      "AT#CACTL?",
      "AT#CAINFO?",
      "AT#RFSTS",
  };

  bool isExecute = true;
  for (i = 0; i < NUM_ELEMS(diagATCmd); i++) {
    if (0 == strcmp(*(diagATCmd + i), "AT#CACTL?"))
      isExecute = isLteA;
    else if (0 == strcmp(*(diagATCmd + i), "AT#CAINFO?")) {
      if (isLteA) {
        requestShowCAInformation();
        isExecute = false;
      }
    } else if (0 == strcmp(*(diagATCmd + i), "AT#RFSTS")) {
      requestReadCurrentNetworkStatus(isLteA);
      isExecute = false;
    }

    if (isExecute)
      at_send_command(*(diagATCmd + i), &p_response);
  }

  return RIL_E_SUCCESS;
}

RIL_Errno requestReadNetworkStatus(void) {
  switch (get_module_type()) {
    case TELIT_LN940A9_FAMILY:
    case TELIT_LN960A16_FAMILY:
      return _requestReadNetworkStatusLNSeries();
      break;
    case TELIT_LM940A11_FAMILY:
    case TELIT_LM960A18_FMAILY:
      return _requestReadNetworkStatusLegacy(true);
      break;
    default:
      return _requestReadNetworkStatusLegacy(false);
      break;
  }
}

/*
 *  Function: Read RX Power Level
 */

#define RXRL_TESTMODE_TIMES 5

bool _isBandSupportLNSeries(bool isLTE, int bnd) {
  int err;
  ATResponse *p_response = NULL;
  ATLine *p_cur;

  err = at_send_command_multiline("AT^SLBAND=?", "^SLBAND:", &p_response);
  if (err < 0 || p_response->success == 0) goto error;

  for (p_cur = p_response->p_intermediates; p_cur != NULL;
       p_cur = p_cur->p_next) {
    char *line = p_cur->line;
    char *tech;
    int lteband = 0;
    bool isFirstTok = true;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextstr(&line, &tech);
    if (err < 0) goto error;

    while (at_tok_hasmore(&line)) {
      if (isFirstTok) {
        char *band;
        err = at_tok_nextstr(&line, &band);
        lteband = (int)strtol(band + 1, NULL, 10);
      } else
        err = at_tok_nextint(&line, &lteband);
      if (err < 0) goto error;

      if (lteband == bnd) {
        if (isLTE && (0 == strcmp(tech, "LTE")))
          return true;
        if (!isLTE && (0 == strcmp(tech, "WCDMA")))
          return true;
      }
      isFirstTok = false;
    }
  }

  return false;

error:
  return false;
}

const char *prefixRXPowerLevelLNSeries[] = {
    "",
    "",
    "",
    "",

    "LTE, main RSSI:",
    "LTE, auxiliary RSSI:",
    "",
    "",

    "WCDMA, MAIN RSSI:",
    "WCDMA, AUX1 RSSI:",
    "",
    "",

    "LTE, MAIN RSSI:",
    "LTE, AUX1 RSSI:",
    "LTE, AUX2 RSSI:",
    "LTE, AUX3 RSSI:",
};

RIL_Errno _requestReadRXPowerLevelLNSeries(bool isLTE, bool isFourAnt, int bnd) {
  int err;
  ATResponse *p_response = NULL;
  int i, j;
  int rxlevel[4][RXRL_TESTMODE_TIMES];
  int read_level = 0;
  char cmd[MY_PARAM_MIN];
  const char *prefix;
  int paths = (isFourAnt) ? 4 : 2;
  int base = (isFourAnt) ? 8 : 0;

  if (_isBandSupportLNSeries(isLTE, bnd)) {
    at_send_command("AT+FTM=1", &p_response);
    usleep(250000);

    for (i = 0; i < paths; i++) {
      for (j = 0; j < RXRL_TESTMODE_TIMES; j++) {
        if (isFourAnt)
          snprintf(cmd, MY_PARAM_MIN, "AT+AGC=%d,%d,%d,0,%d,3,3",
                   (isLTE) ? 2 : 1, bnd, get_rxrl_earfcn_ul(bnd), (i + 1));
        else
          snprintf(cmd, MY_PARAM_MIN, "AT+AGC=%d,%d,%d,0,%d,3,3",
                   (isLTE) ? 3 : 1, bnd, get_rxrl_earfcn_ul(bnd), i);
        err = at_send_command(cmd, &p_response);
        if (err < 0 || p_response->success == 0) goto error;
        usleep(250000);

        prefix = *(prefixRXPowerLevelLNSeries + i + (isLTE * 1 << 2) + base);
        err = at_send_command_singleline("AT+AGC?", prefix, &p_response);
        if (err < 0 || p_response->success == 0) goto error;

        char *line = p_response->p_intermediates->line;
        err = at_tok_start(&line);
        if (err < 0) goto error;

        err = at_tok_nextint(&line, &read_level);
        if (err < 0) goto error;

        rxlevel[i][j] = read_level;
      }
    }

    at_send_command("AT+FTM=0", &p_response);
  } else {
    printf("%s B%d not support!!\n", (isLTE) ? "LTE" : "WCDMA", bnd);
    goto error;
  }

  for (i = 0; i < paths; i++) {
    int sum = 0;
    if (0 == i)
      printf("%s, MAIN RSSI: [", (isLTE) ? "LTE" : "WCDMA");
    else
      printf("%s, AUX%d RSSI: [", (isLTE) ? "LTE" : "WCDMA", i);
    for (j = 0; j < RXRL_TESTMODE_TIMES; j++) {
      printf(" %3d", rxlevel[i][j]);
      sum += rxlevel[i][j];
    }
    printf(" ], AVG = %3.1f\n", (float)sum / RXRL_TESTMODE_TIMES);
  }

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

bool _isBandSupportLegacy(int bnd) {
  int err;
  ATResponse *p_response = NULL;

  char *line;
  char *skip;
  long mask = 0;
  long lteband = 0;

  err = at_send_command_singleline("AT#BND?", "#BND:", &p_response);
  if (err < 0 || p_response->success == 0) goto error;

  line = p_response->p_intermediates->line;
  err = at_tok_start(&line);
  if (err < 0) goto error;

  err = at_tok_nextstr(&line, &skip);
  if (err < 0) goto error;

  err = at_tok_nextstr(&line, &skip);
  if (err < 0) goto error;

  err = at_tok_nexthexlong(&line, &lteband);
  if (err < 0) goto error;

  mask = ((unsigned long)1) << (bnd - 1);
  return ((lteband & mask) > 0) ? true : false;

error:
  return false;
}

RIL_Errno _requestReadRXPowerLevelLegacy(int bnd) {
  int err;
  ATResponse *p_response = NULL;
  int i, j = 0;
  int rxrl[2][RXRL_TESTMODE_TIMES] = {0};

  char rxrlATCmd[][MY_PARAM_MIN] = {
      "AT#TESTMODE=\"TM\"",
      "AT#TESTMODE=\"SETLTEBAND",
      "AT#TESTMODE=\"LTXBW\"",
      "AT#TESTMODE=\"LRXBW\"",
      "AT#TESTMODE=\"CH",
      "AT#TESTMODE=\"LNA4G\"",
      "AT#TESTMODE=\"PRXRL4G\"",
      "AT#TESTMODE=\"DRXRL4G\"",
      "AT#TESTMODE=\"OM\"",
      //"AT#TESTMODE=\"ESC\"",
  };

  if (_isBandSupportLegacy(bnd)) {
    snprintf(rxrlATCmd[1], MY_PARAM_MIN, "AT#TESTMODE=\"SETLTEBAND %d\"", bnd);
    snprintf(rxrlATCmd[4], MY_PARAM_MIN, "AT#TESTMODE=\"CH %d\"", get_rxrl_earfcn_ul(bnd));
  }

  for (i = 0; i < NUM_ELEMS(rxrlATCmd); i++) {
    if (NULL != strstr(*(rxrlATCmd + i), "RXRL4G")) {
      char *line;
      int read_level = 0;
      char prefix[] = "PRXRL4G:";
      prefix[0] = (*(rxrlATCmd + i))[13];

      err = at_send_command_singleline(*(rxrlATCmd + i), prefix, &p_response);
      if (err < 0 || p_response->success == 0) goto error;

      line = p_response->p_intermediates->line;
      err = at_tok_start(&line);
      if (err < 0) goto error;

      err = at_tok_nextint(&line, &read_level);
      if (err < 0) goto error;

      rxrl[i % 2][j] = read_level;

      j++;
      if (j < RXRL_TESTMODE_TIMES)
        i--;
      else
        j = 0;
    } else {
      at_send_command(*(rxrlATCmd + i), &p_response);
      if (0 == i)
        usleep(250000);
    }
  }

  int sum = 0;
  for (i = 0; i < RXRL_TESTMODE_TIMES; i++)
    sum += rxrl[0][i];
  printf("%.1f\n", (float)(sum / RXRL_TESTMODE_TIMES));

  sum = 0;
  for (i = 0; i < RXRL_TESTMODE_TIMES; i++)
    sum += rxrl[1][i];
  printf("%.1f\n", (float)(sum / RXRL_TESTMODE_TIMES));

  return RIL_E_SUCCESS;

error:
  return RIL_E_GENERIC_FAILURE;
}

RIL_Errno requestReadRXPowerLevel(bool isLTE, int bnd) {
  switch (get_module_type()) {
    case TELIT_LN940A9_FAMILY:
    case TELIT_LN960A16_FAMILY:
      return _requestReadRXPowerLevelLNSeries(isLTE, get_module_type() == TELIT_LN960A16_FAMILY, bnd);
      break;
    case TELIT_LM940A11_FAMILY:
    case TELIT_LM960A18_FMAILY:
      return _requestReadRXPowerLevelLegacy(bnd);
      break;
    default:
      return _requestReadRXPowerLevelLegacy(bnd);
      break;
  }
}
