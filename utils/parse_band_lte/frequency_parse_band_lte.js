const cheerio = require('cheerio');
const request = require('request');
var util = require('util');

//const url = 'http://niviuk.free.fr/lte_band.php';
const url_bandlist = 'https://www.sqimway.com/lte_band.php';
const url_bandwidth = 'https://www.sqimway.com/lte_bandwidth.php';

var is_tdd = 0;
let bwlist = [];

request(url_bandwidth, (err, res, body) => {
  const $ = cheerio.load(body);
  $('.table_bw tbody').first().find('tr').each(function (i, elem) {
    var bw = 0;
    var band = parseInt($(this).find('th').first().text());
    for (i = 7; i < 13; i++) {
      var bwdom = $(this).find('td').eq(i);
      if ((bwdom.hasClass('td_color2')) || (bwdom.hasClass('tr_color3')))
        bw += (1 << (i - 7));
    }
    bwlist[band] = bw;
  });

  request(url_bandlist, (err, res, body) => {
    const $ = cheerio.load(body);
    $('.table_bw tbody').first().find('tr').each(function (i, elem) {
      var band = $(this).find('th').eq(0).text();
      var duplex_spacing = $(this).find('td').eq(6).text();
      var dl_earfcn = $(this).find('td').eq(1);
      var ul_earfcn = $(this).find('td').eq(4);
      var dl_freq_min = dl_earfcn.html().split("<br>")[0];
      var dl_earfcn_min = dl_earfcn.find('.ssf1').text();
      var dl_earfcn_max = $(this).find('td').eq(2).find('.ssf1').text();
      var ul_earfcn_min = ul_earfcn.find('.ssf1').text();
      var ul_earfcn_max = $(this).find('td').eq(5).find('.ssf1').text();
      var mode = 'DUPLEX_MODE_FDD';
      var bw = (typeof bwlist[parseInt(band)] === 'undefined') ? 0 : bwlist[parseInt(band)];

      if (is_tdd) {
        ul_earfcn_min = 0;
        ul_earfcn_max = 0;
        duplex_spacing = 0;
        mode = 'DUPLEX_MODE_TDD';
      }
      if (ul_earfcn.text() == "Downlink only") {
        ul_earfcn_min = 0;
        ul_earfcn_max = 0;
        duplex_spacing = 0;
        mode = 'DUPLEX_MODE_SDL';
      }

      if (dl_freq_min != 'TDD') {
        var result = util.format('{%s,\n%s,\n%s,\n%s,\n%s,\n%s,\n%s,\n%s,\n%d},',
          band, duplex_spacing, dl_freq_min, dl_earfcn_min, dl_earfcn_max, ul_earfcn_min, ul_earfcn_max, mode, bw);

        console.log(result);
      } else
        is_tdd = 1;
    });
  })

  console.log(bwlist);
  return bwlist;
});
