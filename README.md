# telit-at-controller

### Telit AT controller tool under Linux

Download the source code and build the application

```sh
~$ git clone https://gitlab.com/subnike.tw/telit-at-controller
~$ cd telit-at-controller
~/telit-at-controller$ CROSS_COMPILE=arm-linux-gnueabihf- make
```

Example - Send AT command to Telit's module automatically

```sh
$ ./bin/at-controller -a AT+CGMM
AT+CGMM

LM940

OK
```

Example - List all Telit's module on the platform and print out the mapping ports

```sh
$ ./bin/at-controller -l
Id=0, Module=LE922A/LM9x0 Vendor=1bc7 ProdID=1040 Cfg#=1/1
   |__ If AtCmd = /dev/ttyUSB2
   |__ If Modem = /dev/ttyUSB3
   |__ If Debug = /dev/ttyUSB0
   |__ If NMEA  = /dev/ttyUSB1
   |__ If MBIM  = /dev/cdc-wdm1
```
