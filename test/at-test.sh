#!/bin/sh

LOG_PATH=$(dirname -- "$(realpath $0)")
LOG_FILE="$LOG_PATH/log_at-controller_"`date "+%Y%m%d-%H%M%S"`".txt"
EXE_PATH=$(realpath "$LOG_PATH/../bin")
EXE_FILE="$EXE_PATH/Telit-AT-Controller"
MAXIMUM_LOOP=2

function print_msg () {
  time=`date "+%Y-%m-%d %H:%M:%S"`
  echo "[${time}] $1" | tee -a $LOG_FILE
}

function ctrl_c_pressed {
  print_msg "Ctrl+C Pressed."
  print_msg "========== AT-Controller AT Command Test Break =========="
  exit 0
}
trap 'ctrl_c_pressed' SIGINT  # set ctrl+C handler

if [ ! -f "$EXE_FILE" ]; then
  print_msg "at-controller not exits"
  exit 1
fi

print_msg "========== AT-Controller AT Command Test Start =========="
COUNT=1
while [ $COUNT -le $MAXIMUM_LOOP ]; do
  print_msg "***** Test Round: $COUNT *****"
  "$EXE_FILE" -t 1000 -a ATI >> $LOG_FILE
  sleep .1
  COUNT=`expr $COUNT + 1`
done

print_msg "========== AT-Controller AT Command Test Stop =========="
