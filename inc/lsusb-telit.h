#ifndef _LSUSB_TELIT_H
#define _LSUSB_TELIT_H

struct GEnumValueShort {
  int value;
  const char *value_name;
};

typedef enum {
  unknown,
  TELIT_ME910C1_FAMILY,
  TELIT_MEx10G1_FAMILY,
  TELIT_LE910Bx_FMAILY,
  TELIT_LE910Cx_FAMILY,
  TELIT_LM940A11_FAMILY,
  TELIT_LM960A18_FMAILY,
  TELIT_LN940A9_FAMILY,
  TELIT_LN960A16_FAMILY,
  TELIT_FN980_FAMILY,
} MMBoardbandModem_Telit;

void print_usb_devices(void);
char *get_module_description(void);
char *get_module_product(void);
MMBoardbandModem_Telit get_module_type(void);
void get_atcmd_port(char *buf, unsigned int m, unsigned int p);
void get_netintf(char *buf, unsigned int m);
int lsusb_telit(void);

#endif /* _LSUSB_TELIT_H */
