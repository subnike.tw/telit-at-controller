#ifndef _LTS_FREQUENCY_BAND_H
#define _LTS_FREQUENCY_BAND_H

unsigned int get_ul_earfcn(unsigned int dl);
unsigned int get_rxrl_earfcn_ul(int bnd);
const char *getGSMBandCapabilityString(long bnd);
const char *getUMTSBandCapabilityString(long bnd);

#endif /* _LTS_FREQUENCY_BAND_H */
