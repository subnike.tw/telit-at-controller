#ifndef _REFERENCE_RIL_H
#define _REFERENCE_RIL_H

#define MY_PARAM_MIN 32
#define MY_PARAM_MAX 64

typedef enum {
  RIL_E_SUCCESS = 0,
  RIL_E_GENERIC_FAILURE = 2,
} RIL_Errno;

struct BAND_CAPABILITY_T {
  u_int8_t numLTE;
  u_int16_t bndLTE[MY_PARAM_MAX];
  u_int8_t numNSA;
  u_int16_t bndNSA[MY_PARAM_MAX];
  u_int8_t numSA;
  u_int16_t bndSA[MY_PARAM_MAX];
} __attribute__((packed));

RIL_Errno requestCustomATCmd(const char *cmd);
RIL_Errno requestBandCapability(void);
RIL_Errno requestReadCurrentNetworkStatus(bool isLteA);
RIL_Errno requestShowCAInformation(void);
// for MACRO_DISABLE_WCDMA
RIL_Errno requestSelectMode(void);
RIL_Errno requestSelectBand(void);
RIL_Errno requestDisableWCDMA(void);
// for MACRO_FACROTY_SIMTEST
RIL_Errno requestFactorySimTesting(void);
// for MACRO_READ_NETWORK_STATE
RIL_Errno requestReadNetworkStatus(void);
// for MACRO_READ_RX_POWER_LEVEL
RIL_Errno requestReadRXPowerLevel(bool isLTE, int bnd);

#endif /* _REFERENCE_RIL_H */
