#ifndef LOG_H_
#define LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#define LOG_LEVEL_ERROR 0
#define LOG_LEVEL_INFO 1
#define LOG_LEVEL_VERBOSE 2

void set_log_level(int level);
void set_syslog(bool enable);
void set_log_timestamp(bool enable);
void dbg_print(int level, const char *fmt, ...);
void log_progress();

#if !defined(ANDROID_CHANGES)
#define ALOGI(format, ...)                                         \
  do {                                                             \
    char buff[512];                                                \
    sprintf(buff, "%s(%d): %s\n", __FUNCTION__, __LINE__, format); \
    dbg_print(LOG_LEVEL_INFO, buff, ##__VA_ARGS__);                \
  } while (0)
#define ALOGE(format, ...)                                         \
  do {                                                             \
    char buff[512];                                                \
    sprintf(buff, "%s(%d): %s\n", __FUNCTION__, __LINE__, format); \
    dbg_print(LOG_LEVEL_ERROR, buff, ##__VA_ARGS__);               \
  } while (0)
#define ALOGD(format, ...)                                         \
  do {                                                             \
    char buff[512];                                                \
    sprintf(buff, "%s(%d): %s\n", __FUNCTION__, __LINE__, format); \
    dbg_print(LOG_LEVEL_VERBOSE, buff, ##__VA_ARGS__);             \
  } while (0)
#define ALOGW(format, ...)                                         \
  do {                                                             \
    char buff[512];                                                \
    sprintf(buff, "%s(%d): %s\n", __FUNCTION__, __LINE__, format); \
    dbg_print(LOG_LEVEL_INFO, buff, ##__VA_ARGS__);                \
  } while (0)
#endif

#ifdef __cplusplus
} /* C */
#endif

#endif /* LOG_H_ */
