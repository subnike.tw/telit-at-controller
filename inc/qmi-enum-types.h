#ifndef __LIBQMI_GLIB_ENUM_TYPES_H__
#define __LIBQMI_GLIB_ENUM_TYPES_H__
#include "qmi-enums-nas.h"

struct GEnumValue {
  int value;
  const char *value_name;
  const char *value_nick;
};

const char *qmi_nas_active_band_get_string(int val);
const char *qmi_nas_dl_bandwidth_get_string(QmiNasDLBandwidth val);
const char *qmi_nas_network_service_domain_get_string(QmiNasNetworkServiceDomain val);
const char *qmi_nas_modulation_get_string(QmINasModulation val);

#endif /* __LIBQMI_GLIB_ENUM_TYPES_H__ */
