# SPDX-License-Identifier: GPL-2.0
VERSION = 1
PATCHLEVEL = 3
SUBLEVEL = 0
BUILD_NUMBER = $(VERSION)$(if $(PATCHLEVEL),.$(PATCHLEVEL)$(if $(SUBLEVEL),.$(SUBLEVEL)))$(EXTRAVERSION)

.SUFFIXES:
.SUFFIXES: .c .h .o

# project set up and directories
CC = $(CROSS_COMPILE)gcc
STRIP = $(CROSS_COMPILE)strip
LIBS = -lm

INCLDIR	= inc
BINDIR = bin
OBJDIR = obj
SRCDIR = src

# final executable name
_BIN	= Telit-AT-Controller
BIN	= $(addprefix $(BINDIR)/, $(_BIN))

# compilation flags
CFLAGS = -g -Wall -I$(INCLDIR) -DVERSION_NUMBER=\"$(BUILD_NUMBER)\"
OFLAGS =
LDFLAGS = -pthread

SRCS	= $(wildcard $(SRCDIR)/*.c)
_OBJS	= $(patsubst src/%.c, %.o, $(SRCS))
OBJS	= $(addprefix $(OBJDIR)/, $(_OBJS))

.PHONY: default all clean

default: $(BIN)
all: default

$(BIN): $(OBJS) $(BINDIR)
	$(CC) $(OBJS) -Wall $(LIBS) -o $@ $(LDFLAGS)
	$(STRIP) -s $@

$(BINDIR):
	mkdir -p $(BINDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(OBJDIR) 
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJDIR):
	mkdir -p $(OBJDIR)

.PRECIOUS: $(BIN) $(OBJS)

clean:
	-rm -rf $(OBJDIR)
	-rm -rf $(BINDIR)
